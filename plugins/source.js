import source from '@/assets/data/source'
export default ({ app }, inject) => {
  inject('source', source)
}
