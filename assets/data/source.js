export default {
  products: [
    {
      category: 'Category 1',
      items: [
        {
          title: 'CAT 1 - PROD 1',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/257736/pexels-photo-257736.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        },
        {
          title: 'CAT 1 - PROD 2',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/236089/pexels-photo-236089.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        },
        {
          title: 'CAT 1 - PROD 3',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/1432675/pexels-photo-1432675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        }
      ]
    },
    {
      category: 'Category 2',
      items: [
        {
          title: 'CAT 2 - PROD 1',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/159397/solar-panel-array-power-sun-electricity-159397.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        },
        {
          title: 'CAT 2 - PROD 2',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/110844/pexels-photo-110844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        },
        {
          title: 'CAT 2 - PROD 3',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/2865025/pexels-photo-2865025.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        },
        {
          title: 'CAT 2 - PROD 4',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/1145434/pexels-photo-1145434.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        }
      ]
    },
    {
      category: 'Category 3',
      items: [
        {
          title: 'CAT 3 - PROD 1',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/159220/printed-circuit-board-print-plate-via-macro-159220.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        },
        {
          title: 'CAT 3 - PROD 2',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/1148820/pexels-photo-1148820.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        },
        {
          title: 'CAT 3 - PROD 3',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        },
        {
          title: 'CAT 3 - PROD 4',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/442150/pexels-photo-442150.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        },
        {
          title: 'CAT 3 - PROD 5',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/163073/raspberry-pi-computer-linux-163073.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        },
        {
          title: 'CAT 3 - PROD 6',
          body:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum aliquet hendrerit. Integer congue leo.',
          headline: 'Lorem ipsum dolor sit amet',
          src:
            'https://images.pexels.com/photos/1624895/pexels-photo-1624895.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        }
      ]
    }
  ],
  services: [
    {
      src:
        'https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      label: 'Tu servicio 1'
    },
    {
      src:
        'https://images.pexels.com/photos/33109/fall-autumn-red-season.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      label: 'Tu servicio 2'
    },
    {
      src:
        'https://images.pexels.com/photos/371589/pexels-photo-371589.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      label: 'Tu servicio 3'
    },
    {
      src:
        'https://images.pexels.com/photos/709552/pexels-photo-709552.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      label: 'Tu servicio 4'
    }
  ],
  us: {
    body: `Somos una empresa de calidad mundial, nuestros clientes salen contentos y seguros del beneficio que nuestros servicios traen a sus negocios.`
  },
  mission: {
    body: `Somos una empresa de calidad mundial, nuestros clientes salen contentos y seguros del beneficio que nuestros servicios traen a sus negocios.`
  },
  vision: {
    body: `Somos una empresa de calidad mundial, nuestros clientes salen contentos y seguros del beneficio que nuestros servicios traen a sus negocios.`
  },
  floatingImage:
    'http://www.aerre2.com/wp-content/uploads/2016/06/aerre2-dreinage-monophase-prodotto-3.png',
  palette: {
    primary: '#12324E',
    title: '#3B4A66',
    body: '#74788E',
    accent: '#FDA40E'
  }
}
